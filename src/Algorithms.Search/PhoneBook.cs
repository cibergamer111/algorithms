﻿namespace Algorithms.Search;
public class PhoneBook
{
    private readonly Dictionary<string, string> phoneBook = [];

    public void AddContact(string name, string number)
    {
        phoneBook.Add(name, number);
    }

    /// <summary>
    /// Complexity O(1)
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public string FindNumberByName(string name)
    {
        return phoneBook[name];
    }

    /// <summary>
    /// Complexity O(n)
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public string FindNameByNumber(string number)
    {
        return phoneBook
            .First(pb => pb.Value == number)
            .Key;
    }
}
