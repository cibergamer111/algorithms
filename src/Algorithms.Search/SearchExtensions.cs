﻿namespace Algorithms.Search;
public static class SearchExtensions
{
    public static int BinarySearch(int[] list, int item)
    {
        Console.WriteLine($"Поиск числа '{item}'.");

        int lowIndex = 0;
        int highIndex = list.Length - 1;
        int iteration = 1;

        while (lowIndex <= highIndex)
        {
            Console.WriteLine($"Итерация {iteration++}.");

            int middleIndex = (lowIndex + highIndex) / 2;
            var guess = list[middleIndex];

            if (guess == item)
            {
                Console.WriteLine($"Число найдено по индексу '{middleIndex}'.");
                return middleIndex;
            }

            if (guess < item)
            {
                lowIndex = middleIndex + 1;
            }
            else
            {
                highIndex = middleIndex - 1;
            }
        }

        throw new InvalidOperationException($"Item '{item}' not found.");
    }

    public static int FindSmallestIndex(int[] array)
    {
        var smallest = array[0];
        var smallestIndex = 0;

        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] < smallest)
            {
                smallest = array[i];
                smallestIndex = i;
            }
        }

        return smallestIndex;
    }
}
