﻿namespace Algorithms.Search;
public class DuplicateChecker
{
    private readonly HashSet<string> storage = [];

    /// <summary>
    /// Complexity O(1)
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool TryAdd(string value)
    {
        if (storage.Contains(value))
        {
            return false;
        }

        storage.Add(value);
        return true;
    }
}
