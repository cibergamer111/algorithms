﻿using Algorithms.DataStructures;

namespace Algorithms.Search;
public class BreadthFirstSearch
{
    private readonly Queue<string> queue = [];
    private readonly HashSet<string> checkedItems = [];

    public string Find(Graph graph, string startNode)
    {
        if (startNode.EndsWith('M'))
        {
            return startNode;
        }

        checkedItems.Add(startNode);

        var neighbors = graph.GetNeighbors(startNode);
        foreach (var neighbor in neighbors)
        {
            queue.Enqueue(neighbor);
        }

        string? next;
        while (true)
        {
            next = queue.Dequeue();

            if (next is null)
                throw new InvalidOperationException("Element not found");

            if (!checkedItems.Contains(next))
                break;
        }

        return Find(graph, next);
    }
}
