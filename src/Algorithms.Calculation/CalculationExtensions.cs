﻿namespace Algorithms.Calculation;
public static class CalculationExtensions
{
    public static int FactorialViaRecursion(int value)
    {
        if (value == 1)
            return 1;

        return value * FactorialViaRecursion(value - 1);
    }

    public static int FactorialViaCycle(int value)
    {
        int result = 1;
        for (int i = 1; i <= value; i++)
        {
            result *= i;
        }

        return result;
    }
}
