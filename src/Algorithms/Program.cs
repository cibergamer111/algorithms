﻿using Algorithms.DataStructures;
using Algorithms.Search;
using static Algorithms.Calculation.CalculationExtensions;
using static Algorithms.Search.SearchExtensions;
using static Algorithms.Sort.SortExtensions;

//Binary search
Console.WriteLine("БИНАРНЫЙ ПОИСК");
int[] list = [3, 5, 8, 10, 12, 15, 17, 18, 21, 30];

try
{
    BinarySearch(list, 18);
    BinarySearch(list, 30);
    BinarySearch(list, 3);
    BinarySearch(list, 50);
}
catch (InvalidOperationException ex)
{
    Console.WriteLine(ex.Message);
}

try
{
    BinarySearch(list, -1);
}
catch (InvalidOperationException ex)
{
    Console.WriteLine(ex.Message);
}

//Selection sort
Console.WriteLine("\nСОРТИРОВКА ВЫБОРОМ");
int[] array = [17, 2, 45, 38, 4, 11, 24, 76, 85, 3];
Console.WriteLine($"Изначальный массив: ");
Array.ForEach(array, el => Console.Write($"{el}, "));

var sortedArray = SelectionSort(array);
Console.WriteLine($"\nСортированный массив: ");
Array.ForEach(sortedArray, el => Console.Write($"{el}, "));

//Recursion
Console.WriteLine("\n\nРЕКУРСИЯ");
Console.WriteLine($"Факториал (рекурсивно) 8 = {FactorialViaRecursion(8)}.");
Console.WriteLine($"Факториал (через цикл) 8 = {FactorialViaCycle(8)}.");
Console.WriteLine("\n");

//Hash
PhoneBook phoneBook = new();
phoneBook.AddContact("Mary", "+735987861254");
phoneBook.AddContact("Max", "+741547165489");

var number = phoneBook.FindNumberByName("Mary");
Console.WriteLine(number);
var name = phoneBook.FindNameByNumber("+741547165489");
Console.WriteLine(name);

DuplicateChecker checker = new();
if (checker.TryAdd("Max"))
{
    Console.WriteLine("Max добавлен");
}

if (checker.TryAdd("Mary"))
{
    Console.WriteLine("Mary добавлена");
}

if (checker.TryAdd("Max"))
{
    Console.WriteLine("Max добавлен");
}
else
{
    Console.WriteLine("Max уже есть");
}

Console.WriteLine();
HashTable hashTable = new();
hashTable.Add("Audi", "V6");
hashTable.Add("VolksWagen", "R4");
hashTable.Add("Ferrary", "V12");

Console.WriteLine(hashTable.Find("Audi"));
Console.WriteLine(hashTable.Find("Ferrary"));

//Breadth first search and graph
Console.WriteLine("\nBreadth first search and graph");
Graph graph = new();
graph.Add("Max", "Mary");
graph.Add("Mary", "SergeyM", "Anna");
graph.Add("Max", "Nikolay", "Alexandra");

BreadthFirstSearch search = new();
var foundValue = search.Find(graph, "Max");
Console.WriteLine(foundValue);

Console.WriteLine();
int[] array2 = [17, 2, 45, 38, 4, 11, 24, 76, 85, 3];
var sortedArray2 = QuickSort(array2);
sortedArray2.ForEach(el => Console.Write($"{el}, "));

