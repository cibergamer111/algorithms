﻿using static Algorithms.Search.SearchExtensions;

namespace Algorithms.Sort;
public static class SortExtensions
{
    public static int[] SelectionSort(int[] array)
    {
        int[] resultArray = new int[array.Length];

        for (int i = 0; i < resultArray.Length; i++)
        {
            var smallestIndex = FindSmallestIndex(array);
            resultArray[i] = array[smallestIndex];
            array[smallestIndex] = int.MaxValue;
        }

        return resultArray;
    }

    public static List<int> QuickSort(IList<int> array)
    {
        if (array.Count < 2)
        {
            return array.ToList();
        }

        var pivotIndex = new Random().Next(array.Count - 1);
        Console.WriteLine($"Index is {pivotIndex}");
        var pivot = array[pivotIndex];
        List<int> left = [];
        List<int> right = [];
        List<int> middle = [];
        foreach (var item in array)
        {
            if (item < pivot)
            {
                left.Add(item);
            }
            else if (item > pivot)
            {
                right.Add(item);
            }
            else
            {
                middle.Add(item);
            }
        }

        return [.. QuickSort(left), .. middle, .. QuickSort(right)];
    }
}
