﻿namespace Algorithms.DataStructures;
public class HashTable
{
    private const int size = 1000;
    private readonly Entry[] storage = new Entry[size];

    public void Add(string key, string value)
    {
        var hashCode = GetHashCode(key);

        if (storage[hashCode] is null)
        {
            storage[hashCode] = new(key, value);
        }
        else
        {
            storage[hashCode].Next = new(key, value);
        }
    }

    public string Find(string key)
    {
        var hashCode = GetHashCode(key);
        return storage[hashCode].Value;
    }

    private int GetHashCode(string key)
    {
        var hashCode = key.GetHashCode();
        return Math.Abs(hashCode % size);
    }

    private class Entry
    {
        public Entry(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; private set; }
        public string Value { get; private set; }
        public Entry Next { get; set; }
    }
}
