﻿namespace Algorithms.DataStructures;
public class Graph
{
    private readonly Dictionary<string, List<string>> storage = [];

    public void Add(string node, params string[] linkedNodes)
    {
        if (storage.ContainsKey(node))
        {
            storage[node].AddRange(linkedNodes);
        }
        else
        {
            storage.Add(node, linkedNodes.ToList());
        }

        foreach (var linkedNode in linkedNodes)
        {
            storage.Add(linkedNode, []);
        }
    }

    public List<string> GetNeighbors(string node)
    {
        return storage[node];
    }
}
